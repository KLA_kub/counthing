//
//  HistoryTableView.swift
//  Counthing
//
//  Created by Blackwolf on 6/23/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

import UIKit

class HistoryTableView: UITableViewController {
    
    let items = ["Item 1", "Item2", "Item3", "Item4"]
    let items2 = ["1", "2", "3", "4"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: "Customcell", bundle: nil), forCellReuseIdentifier: "CustomOne")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        colorja()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:Customcell = tableView.dequeueReusableCellWithIdentifier("CustomOne", forIndexPath: indexPath) as! Customcell

        cell.NameLabel.text = items[indexPath.row]
        cell.countLable.text = items2[indexPath.row]
        cell.imageLabel.image = UIImage(named:"Green-Leaves-Wallpaper-Backgrounds.png")
        cell.backgroundColor = UIColor.clearColor()
        
        cell.imageLabel.layer.cornerRadius = 8.0
        cell.imageLabel.clipsToBounds = true
        cell.blrV.layer.cornerRadius = 8.0
        cell.blrV.clipsToBounds = true
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func colorja(){
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors =
            [UIColor(red:33.0/255, green:0.0/255, blue: 58.0/255, alpha:1).CGColor,
             UIColor(red:66.0/255, green:1.0/255, blue: 115.0/255, alpha:1).CGColor]
        
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().fixedCoordinateSpace.bounds.width, height: UIScreen.mainScreen().fixedCoordinateSpace.bounds.height)
        
        self.view.layer.insertSublayer(gradient, atIndex: 0)
    }

    
}
