//
//  OpenCVWrapper.m
//  Counthing
//
//  Created by Blackwolf on 6/15/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

#import "OpenCVWrapper.h"
#import <opencv2/opencv.hpp>
#import <opencv2/core/core_c.h>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/imgcodecs/ios.h>
#import <opencv2/highgui/highgui_c.h>

using namespace cv;
@implementation OpenCVWrapper

int lastedResult = 0;
std::vector<int> resultList;
std::vector<std::vector<Point2i>> contours;

// get opencv function string Objective-C
+(NSString *) openCVVersionString
{
    return [NSString stringWithFormat:@"OpenCV Version %s",CV_VERSION];
}

+(UIImage *) makeGrayFromImage:(UIImage *)image
{
    Mat imageMat;
    UIImageToMat(image,imageMat);
    
    if(imageMat.channels() == 1 ) return image;
    
    
    Mat grayMat;
    cvtColor(imageMat,grayMat,CV_BGR2GRAY);
    
    return MatToUIImage(grayMat);
}


+(UIImage *) dynamicBGCutting:(UIImage *)image
{
    Mat imageTemp;
    UIImageToMat(image, imageTemp);
    
    Mat calImage;
    cvtColor(imageTemp,calImage,CV_BGR2GRAY);
    resize(calImage,calImage,Size2d(256,256));
  
    int sampling = (int)calImage.data[calImage.step*1+1];
    
    for(int i = 0; i < calImage.rows; i++){
        for(int j=0; j < calImage.cols; j++){
            
            if(calImage.data[calImage.step*j+i] < sampling+20 && calImage.data[calImage.step*j+i] > sampling-20){
                
                calImage.data[calImage.step*j+i] = 0;
                
                
            }
        }
    }
    
    for(int i = 0; i < calImage.rows*4; i++){
        for(int j=0; j < calImage.cols; j++){
            
            if(calImage.data[calImage.step*j+i] > 0){
                calImage.data[calImage.step*j+i] = 255;
                
            }
        }
    }
    
    
    int erosion_size = 2;
    Mat element = getStructuringElement(cv::MORPH_ELLIPSE,
                                        cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1),
                                        cv::Point(erosion_size, erosion_size) );
    
    Mat temp1,temp2,temp3, temp4;
    dilate(calImage, temp1, element);
    erode(temp1 , temp2, element);
    erode(temp2 , temp3, element);
    erode(temp3 , temp4, element);
    
    return MatToUIImage(temp4);
}




+(UIImage *) backgroundRemoval:(UIImage *)image
{
    ////////////////////////////////////
    // remove shadow by HSV technique
    
    Mat imageTemp;
    UIImageToMat(image, imageTemp);
    
    Point2f src_center(imageTemp.cols/2.0F, imageTemp.rows/2.0F);
    Mat rot_mat = getRotationMatrix2D(src_center, 270, 1.0);
    Mat dst;
    warpAffine(imageTemp, dst, rot_mat, imageTemp.size());
    
    
    Mat hsvImage;
    cvtColor(dst, hsvImage, CV_BGR2HSV);
    Mat channel[3];
    split(hsvImage, channel);
    channel[2] = Mat(hsvImage.rows,hsvImage.cols,CV_8UC1,200);

    merge(channel,3,hsvImage);
    Mat rgbImg;
    cvtColor(hsvImage, rgbImg, CV_HSV2BGR);
    //////////////////////////////////
    
    // normalization image in gray scale
    
    Mat gray(rgbImg.rows,rgbImg.cols,CV_8UC1);
    cvtColor(rgbImg,gray,CV_BGR2GRAY);
    normalize(gray,gray,0,255,NORM_MINMAX,CV_8UC1);
    
    
    
        bitwise_not(gray, gray);
        threshold(gray, gray, 10,255 , THRESH_BINARY);
  
    /////////////////////////////////////
    
    
    // Dilate image
    
    Mat dilateGrad = gray;
    int dilateType = MORPH_ELLIPSE;
    int dilateSize = 4;
    Mat elementDilate = getStructuringElement(dilateType, Size2i(2*dilateSize+1,2*dilateSize+1),Point2i(dilateSize,dilateSize));
    dilate(gray,dilateGrad,elementDilate);
    //////////////////////////////////////
    
    // Flood fill image
    
    Mat floodFilled = Mat::zeros(dilateGrad.rows+2, dilateGrad.cols+2,CV_8U);
    floodFill(dilateGrad,floodFilled,Point2i(0,0),0,0,Scalar(),Scalar(),4+(255 << 8) + FLOODFILL_MASK_ONLY);
    floodFilled = Scalar::all(255) - floodFilled;
    Mat temp;
    floodFilled(Rect2i(1,1,dilateGrad.cols-2,dilateGrad.rows-2)).copyTo(temp);
    floodFilled = temp;
    /////////////////////////////////////
    
    // Erode image
    Mat erodeGrad;
    int erosionType = MORPH_ELLIPSE;
    int erosionSize = 4;
    Mat erosionElement = getStructuringElement(erosionType, Size2i(2*erosionSize+1,2*erosionSize+1),Point2i(erosionSize,erosionSize));
    erode(floodFilled,erodeGrad,erosionElement);
    ////////////////////////////////////
    
    //
    
    
    
    
    
    return MatToUIImage(erodeGrad);
}






+(UIImage *)findObject:(UIImage *)image image2:(UIImage *) image2
{
    Mat imageUse;
    Mat imageDraw;
    Mat imageDraw2;
    Mat imageOut;
    UIImageToMat(image, imageUse);
    UIImageToMat(image2, imageDraw2);
    cvtColor(imageDraw2, imageOut, CV_RGB2BGR);
    //resize(imageOut,imageOut,Size2d(256,256));
    Rect2d bounding_rect;
    std::vector<Vec4i> hierarchy;
    
    
    Point2f src_center(imageUse.cols/2.0F, imageUse.rows/2.0F);
    Mat rot_mat = getRotationMatrix2D(src_center, 90, 1.0);
    Mat dst;
    warpAffine(imageUse, imageUse, rot_mat, imageUse.size());
    
    
    findContours(imageUse,contours,hierarchy,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE );

    int count = 0;
    if(contours.size()>0){
        for(int i = 0 ; i<contours.size();i++){
            
            bounding_rect = boundingRect(contours[i]);
            double a = contourArea(contours[i], false);
            if(a > 5000){
            drawContours(imageUse, contours, i, Scalar(255,255,255),CV_FILLED,8,hierarchy);
            rectangle(imageOut,Point2d(bounding_rect.x,bounding_rect.y),Point2d(bounding_rect.x+bounding_rect.width,bounding_rect.y+bounding_rect.height),Scalar(255,0,0),20);
            putText(imageOut,std::to_string(count+1), Point2d(bounding_rect.x+(bounding_rect.width/2)-10,bounding_rect.y+(bounding_rect.height/2)+10), FONT_HERSHEY_PLAIN, 20, Scalar(0,0,255),20);
                count= count+1;
            }
            
        }
    }
    
    
     lastedResult = count;
    
   
    
    cvtColor(imageOut, imageOut, CV_BGR2RGB);
    
    Point2f src_center2(imageOut.cols/2.0F, imageOut.rows/2.0F);
    Mat rot_mat2 = getRotationMatrix2D(src_center2, 90, 1.0);
    Mat dst2;
    warpAffine(imageOut, imageOut, rot_mat2, imageOut.size());
    
    
    return MatToUIImage(imageOut);
}

+(int) getLastResult
{
    return lastedResult;
}


@end
