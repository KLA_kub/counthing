//
//  View1.swift
//  Counthing
//
//  Created by Blackwolf on 6/17/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

import UIKit

class View1: UIViewController {

    var delegate:SCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layer.shadowColor = UIColor.blackColor().CGColor
        self.view.layer.shadowOpacity = 1
        self.view.layer.shadowOffset = CGSizeZero
        self.view.layer.shadowRadius = 10
        colorja()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    @IBAction func lcbtn(sender: UIButton) {
        delegate?.scrollMove(1)
    }

    func colorja(){
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors =
            [UIColor(red:33.0/255, green:0.0/255, blue: 58.0/255, alpha:1).CGColor,
             UIColor(red:66.0/255, green:1.0/255, blue: 115.0/255, alpha:1).CGColor]
           
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().fixedCoordinateSpace.bounds.width, height: UIScreen.mainScreen().fixedCoordinateSpace.bounds.height)
        
        self.view.layer.insertSublayer(gradient, atIndex: 0)
    }
}
