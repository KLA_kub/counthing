//
//  DisplayViewController.swift
//  Counthing
//
//  Created by Blackwolf on 6/21/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

import UIKit

class DisplayViewController: UIViewController ,UITabBarDelegate ,UITabBarControllerDelegate{

   
 
    @IBOutlet weak var tBar: UITabBar!
    
    
    @IBOutlet weak var blrview: UIVisualEffectView!
    @IBOutlet weak var buttonRetry: UIButton!
    @IBOutlet weak var effectImage: UIImageView!
    
    @IBOutlet weak var count: UILabel!
    @IBOutlet weak var imageShow: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    var imagePush:UIImage = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        //imageShow.image = imagePush
        // Do any additional setup after loading the view.
        colorja()
        
    tBar.delegate = self
       tBar.selectedItem = tBar.items![2] as UITabBarItem
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func BAction(sender: UIButton) {
        let vc : ViewMenu = self.storyboard!.instantiateViewControllerWithIdentifier("start") as! ViewMenu
         vc.point = 2
        vc.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func colorja(){
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors =
            [UIColor(red:2.0/255, green:113.0/255, blue: 161.0/255, alpha:1).CGColor,
             UIColor(red:131.0/255, green:203.0/255, blue: 99.0/255, alpha:1).CGColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().fixedCoordinateSpace.bounds.width, height: UIScreen.mainScreen().fixedCoordinateSpace.bounds.height)
        
        self.view.layer.insertSublayer(gradient, atIndex: 0)
        setShaDown()
        blrview.layer.cornerRadius = 8.0
        blrview.clipsToBounds = true
        buttonRetry.layer.cornerRadius = 8.0
        buttonRetry.clipsToBounds = true
     
    }

    func setShaDown(){
        blrview.layer.shadowColor = UIColor.blackColor().CGColor
        blrview.layer.shadowOpacity = 1
         blrview.layer.shadowOffset = CGSizeZero
         blrview.layer.shadowRadius = 10
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        let vc : ViewMenu = self.storyboard!.instantiateViewControllerWithIdentifier("start") as! ViewMenu
        
       
        if(item.tag == 0){
            vc.point = 0
        }else if(item.tag == 1){
            vc.point = 1
        }else if(item.tag == 2){
          vc.point = 2
        }
        vc.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(vc, animated: false, completion: nil)
    }
    
}
