//
//  History.swift
//  Counthing
//
//  Created by Blackwolf on 6/24/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

import Foundation

class History{
    struct Detail{
        static var image:[UIImage] = Array<UIImage>()
        static var count:[Int] = Array<Int>()
        static var name:[String] = Array<String>()
    }
}