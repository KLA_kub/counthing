//
//  View2.swift
//  Counthing
//
//  Created by Blackwolf on 6/17/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

import UIKit

class View2: UIViewController , UIScrollViewDelegate  {

    var delegate:SCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.view.layer.shadowColor = UIColor.blackColor().CGColor
         self.view.layer.shadowOpacity = 0.25
         self.view.layer.shadowOffset = CGSizeZero
         self.view.layer.shadowRadius = 5
        
        
        
colorja();
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func lcBtn(sender: UIButton) {
        delegate?.scrollMove(1)
    }
  
    
    @IBAction func htBtn(sender: UIButton) {
        delegate?.scrollMove(0)
    }
    
    // MARK: - Navigation0
    func colorja(){
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors =
            [UIColor(red:2.0/255, green:113.0/255, blue: 161.0/255, alpha:1).CGColor,
             UIColor(red:131.0/255, green:203.0/255, blue: 99.0/255, alpha:1).CGColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().fixedCoordinateSpace.bounds.width, height: UIScreen.mainScreen().fixedCoordinateSpace.bounds.height)
        
        self.view.layer.insertSublayer(gradient, atIndex: 0)
    }
}
