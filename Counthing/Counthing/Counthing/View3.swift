//
//  View3.swift
//  Counthing
//
//  Created by Blackwolf on 6/17/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

import UIKit
import AVFoundation
class View3: UIViewController {

    var delegate:SCDelegate?
    
    
    @IBOutlet weak var cntBut: UIButton!
    @IBOutlet weak var viewShow: UIView!
    
    let captureSession = AVCaptureSession()
    let stillImageOutput = AVCaptureStillImageOutput()
    var error: NSError?
    var imageUse:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layer.shadowColor = UIColor.blackColor().CGColor
        self.view.layer.shadowOpacity = 1
        self.view.layer.shadowOffset = CGSizeZero
        self.view.layer.shadowRadius = 10
        
colorja()
        prepareCamera()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func colorja(){
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors =
            [UIColor(red:2.0/255, green:113.0/255, blue: 161.0/255, alpha:1).CGColor,
             UIColor(red:100.0/255, green:193.0/255, blue: 184.0/255, alpha:1).CGColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().fixedCoordinateSpace.bounds.width, height: UIScreen.mainScreen().fixedCoordinateSpace.bounds.height)
        
        self.view.layer.insertSublayer(gradient, atIndex: 0)
    }
    
    
    @IBAction func ctBtn(sender: UIButton) {
       // camActive(){result in
           // self.imageUse = result.result
           // var imageTemp = self.imageUse
            //self.imageUse = OpenCVWrapper.dynamicBGCutting(self.imageUse)
            //self.imageUse = OpenCVWrapper.findObject(self.imageUse, image2:imageTemp);
            //var re = String(OpenCVWrapper.getLastResult())
            
           // self.imageUse = OpenCVWrapper.backgroundRemoval(self.imageUse)
            //self.imageUse = OpenCVWrapper.findObject(self.imageUse, image2: imageTemp)
            //self.delegate?.display(self.imageUse!)
            //var re = String(OpenCVWrapper.getLastResult())
           // print(re)
        //}
        self.imageUse = UIImage(named:"Green-Leaves-Wallpaper-Backgrounds.png")
        self.delegate?.display(self.imageUse!,name: "GG",count: 5)
    }
    
    
    func camActive(completeHandle:(result:UIImage ,error:String)->()){
        if let videoConnection = stillImageOutput.connectionWithMediaType(AVMediaTypeVideo) {
            stillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                
                //self.imageUse = UIImage(data:imageData)

                completeHandle(result:UIImage(data:imageData)!,error:"gg")
                
            }
        }
    }
    
    @IBAction func plsBtn(sender: UIButton) {
        cntBut.setBackgroundImage(UIImage(named:"fnBtn.png"), forState: .Normal)
      
    }
    
   
    
    func prepareCamera(){
        let devices = AVCaptureDevice.devices().filter{ $0.hasMediaType(AVMediaTypeVideo) && $0.position == AVCaptureDevicePosition.Back }
        if let captureDevice = devices.first as? AVCaptureDevice  {
            do{
            try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
            } catch let error as NSError {
                print(error)
            }
            captureSession.sessionPreset = AVCaptureSessionPresetPhoto
    
            captureSession.startRunning()
            stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
            if captureSession.canAddOutput(stillImageOutput) {
                captureSession.addOutput(stillImageOutput)
            }
            if let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession) {
                
                previewLayer.bounds = viewShow.bounds
                //previewLayer.position = CGPointMake(viewShow.bounds.midX, viewShow.bounds.midY)
                previewLayer.frame = CGRect(x: 0.0,y: 0.0,width: viewShow.bounds.width,height: viewShow.bounds.height)
                print("frame \(previewLayer.frame)")
                previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                
                //let cameraPreview = viewShow
                viewShow.layer.addSublayer(previewLayer)
                viewShow.clipsToBounds = true;
                viewShow.layer.addSublayer(cntBut.layer)
                //cameraPreview.addGestureRecognizer(UITapGestureRecognizer(target: self, action:"saveToCamera:"))
                //view.addSubview(cameraPreview)
                
            }
        }
    }
}
