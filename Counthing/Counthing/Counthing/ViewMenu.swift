//
//  ViewMenu.swift
//  Counthing
//
//  Created by Blackwolf on 6/17/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

import UIKit
import RevealingSplashView

protocol SCDelegate{
    func scrollMove(move:Int)
    func display(image:UIImage,name:String,count:Int)
}



class ViewMenu: UIViewController ,UITabBarDelegate ,UITabBarControllerDelegate , UIScrollViewDelegate , SCDelegate{

    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var scrollView: UIScrollView!
    var point:Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let revealingSplashView = RevealingSplashView(iconImage: UIImage(named:"lt4.png")!,iconInitialSize: CGSizeMake(70, 70), backgroundColor: UIColor.greenColor())
        self.view.addSubview(revealingSplashView)
        revealingSplashView.animationType = SplashAnimationType.SqueezeAndZoomOut
        revealingSplashView.startAnimation(){}
        let V1: HistoryTableView = HistoryTableView(nibName:"HistoryTableView",bundle:nil)
        let V2: View2 = View2(nibName:"View2",bundle:nil)
        let V3: View3 = View3(nibName:"View3",bundle:nil)
        
       // V1.delegate = self
        V2.delegate = self
        V3.delegate = self
        
        
        self.addChildViewController(V1)
        self.scrollView.addSubview(V1.view)
        V1.didMoveToParentViewController(self)
        
        self.addChildViewController(V2)
        self.scrollView.addSubview(V2.view)
        V2.didMoveToParentViewController(self)
        
        self.addChildViewController(V3)
        self.scrollView.addSubview(V3.view)
        V3.didMoveToParentViewController(self)
        
        //Reorder layer of element.
        scrollView.bringSubviewToFront(V2.view)
        self.view.bringSubviewToFront(tabBar)
        // Set position of element.
        var V2Frame: CGRect = V2.view.frame
        V2Frame.origin.x = self.view.frame.width
        V2.view.frame = V2Frame
        
        var V3Frame:CGRect = V3.view.frame
        V3Frame.origin.x = 2 * self.view.frame.width
        V3.view.frame = V3Frame
        
        self.scrollView.contentSize = CGSizeMake(self.view.frame.width*3,self.view.frame.size.height)
        
        if(point == 0){
        self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
        }else if(point == 1){
         self.scrollView.contentOffset = CGPoint(x: self.view.frame.width,y: 0)
        }else if(point == 2){
          self.scrollView.contentOffset = CGPoint(x: self.view.frame.width*2,y: 0)
        }
        
        
        tabBar.selectedItem = tabBar.items![point] as UITabBarItem
     
      scrollView.delegate = self
      tabBar.delegate = self
        
        
        
    }
    

    func scrollViewDidEndDecelerating(scrollView: UIScrollView){
        //print(scrollView.contentOffset.x / scrollView.bounds.size.width)
        
        if(scrollView.contentOffset.x / scrollView.bounds.size.width == 0){
            tabBar.selectedItem = tabBar.items![0] as UITabBarItem
        }else if(scrollView.contentOffset.x / scrollView.bounds.size.width == 1){
            tabBar.selectedItem = tabBar.items![1] as UITabBarItem
        }else if(scrollView.contentOffset.x / scrollView.bounds.size.width == 2){
            tabBar.selectedItem = tabBar.items![2] as UITabBarItem
        }
    
    }
    

    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        if(item.tag == 0){
            self.scrollView.setContentOffset(CGPoint(x: 0,y: 0), animated: false)
        }else if(item.tag == 1){
            self.scrollView.setContentOffset(CGPoint(x: self.view.frame.width,y: 0), animated: false)
        }else if(item.tag == 2){
            self.scrollView.setContentOffset(CGPoint(x: self.view.frame.width*2,y: 0), animated: false)
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    
        
    }
    
    
    func scrollMove(move:Int){
        if(move == 0){
           self.scrollView.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
             tabBar.selectedItem = tabBar.items![0] as UITabBarItem
        }else if(move == 1){
            self.scrollView.setContentOffset(CGPoint(x: self.view.frame.width*2,y: 0), animated: true)
           tabBar.selectedItem = tabBar.items![2] as UITabBarItem
        }
    }
    
    
    func display(image:UIImage, name:String,count:Int) {
        let vc : DisplayViewController = self.storyboard!.instantiateViewControllerWithIdentifier("gg") as! DisplayViewController
        vc.imagePush = image
        vc.name.text = name
        vc.count.text = String(count)
        vc.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(vc, animated: false, completion: nil)
    }
    
    
// Action Button
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
