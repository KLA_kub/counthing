//
//  OpenCVWrapper.h
//  Counthing
//
//  Created by Blackwolf on 6/15/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject

// function to get Opencv Version

+(NSString *) openCVVersionString;

+(UIImage *) makeGrayFromImage:(UIImage *)image;

+(UIImage *) dynamicBGCutting:(UIImage *)image;

+(UIImage *) findObject:(UIImage *)image image2:(UIImage *)image2;

+(int) getLastResult;

+(UIImage *) backgroundRemoval:(UIImage *)image;



@end
