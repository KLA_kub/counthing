//
//  ViewController.swift
//  Counthing
//
//  Created by Blackwolf on 6/15/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imagePanel: UIImageView!
    
    @IBOutlet weak var result: UILabel!
    @IBOutlet weak var nameLable: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.        nameLable.text = OpenCVWrapper.openCVVersionString()
        print(imagePanel.image!.size.width)
        print(imagePanel.image!.size.height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonClick(sender: AnyObject) {
        imagePanel.image = OpenCVWrapper.makeGrayFromImage(imagePanel.image)
    }

    @IBAction func buttonCut(sender: UIButton) {
        var img = imagePanel.image;
        imagePanel.image = OpenCVWrapper.dynamicBGCutting(imagePanel.image)
        imagePanel.image = OpenCVWrapper.findObject(imagePanel.image, image2:img);
        result.text = String(OpenCVWrapper.getLastResult())
    }
}

